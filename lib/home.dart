import 'package:flutter/material.dart';
import 'package:travel_app/travelpage.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.orange,
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: const Text("Travel App"),
        ),
        body: const Center(
          child: Text(
            "Welcome to Travel App, Let's find your place",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20.0,
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            elevation: 10.0,
            child: const Icon(Icons.cable),
            onPressed: () {
              // action on button press
            }),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.orangeAccent,
                ),
                child: Text(
                  'Menu',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                  ),
                ),
              ),
              ListTile(
                leading: const Icon(
                  Icons.people,
                ),
                title: const Text('Profile'),
                subtitle: const Text("manage information"),
                onTap: () {},
              ),
              ListTile(
                leading: const Icon(
                  Icons.bookmark,
                ),
                title: const Text('Bookmark'),
                onTap: () {
                  Navigator.pop(context);
                  MaterialPageRoute route =
                      MaterialPageRoute(builder: (value) => const Travelpage());
                  Navigator.push(context, route);
                },
              ),
              ListTile(
                leading: const Icon(
                  Icons.anchor,
                ),
                title: const Text('Setting'),
                onTap: () {},
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: 0,
            fixedColor: Colors.green,
            items: const [
              BottomNavigationBarItem(
                label: "Home",
                icon: Icon(Icons.home),
              ),
              BottomNavigationBarItem(
                label: "Search",
                icon: Icon(Icons.search),
              ),
              BottomNavigationBarItem(
                label: "Profile",
                icon: Icon(Icons.account_circle),
              ),
            ],
            onTap: (int indexOfItem) {}),
      ),
    );
  }
}
