import 'package:flutter/material.dart';
import 'package:travel_app/home.dart';

class Travelpage extends StatefulWidget {
  const Travelpage({super.key});

  @override
  State<Travelpage> createState() => _TravelpageState();
}

class _TravelpageState extends State<Travelpage> {
  final List<TourPackage> tourPackages = [
    TourPackage(
      name: 'Koh larn',
      imageUrl:
          'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/09/a4/7b/40/20151201-101150-largejpg.jpg?w=1200&h=-1&s=1',
      rating: 4.5,
      price: 1999,
    ),
    TourPackage(
      name: 'Chiangmai',
      imageUrl:
          'https://images.contentstack.io/v3/assets/blt00454ccee8f8fe6b/blt60c66f68a92335a9/61bae74a5b7db17e976067e1/CA_ChiangMai_TH_Header.jpg',
      rating: 4.0,
      price: 2599,
    ),
    TourPackage(
      name: 'Khaoyai',
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Khaoyai_panorama2.jpg/1920px-Khaoyai_panorama2.jpg',
      rating: 3.5,
      price: 1499,
    ),
    TourPackage(
      name: 'Phuket',
      imageUrl:
          'https://media-cdn.tripadvisor.com/media/photo-m/1280/1b/4b/5d/c8/caption.jpg',
      rating: 4.2,
      price: 3499,
    ),
    TourPackage(
      name: 'Bangkok',
      imageUrl:
          'https://content.r9cdn.net/rimg/dimg/26/5b/01e97574-city-26166-1592813274a.jpg?width=1366&height=768&xhint=1038&yhint=725&crop=true',
      rating: 4.8,
      price: 899,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Travel App'),
        leading: IconButton(
          icon: const Icon(Icons.home),
          onPressed: () {
            Navigator.pop(context);
            MaterialPageRoute route =
                MaterialPageRoute(builder: (value) => const Home());
            Navigator.push(context, route);
          },
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {},
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: tourPackages.length,
        itemBuilder: (context, index) {
          final tourPackage = tourPackages[index];
          return TourCard(tourPackage: tourPackage);
        },
      ),
    );
  }
}

class TourPackage {
  final String name;
  final String imageUrl;
  final double rating;
  final int price;

  TourPackage({
    required this.name,
    required this.imageUrl,
    required this.rating,
    required this.price,
  });
}

class TourCard extends StatelessWidget {
  final TourPackage tourPackage;

  const TourCard({Key? key, required this.tourPackage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.network(
            tourPackage.imageUrl,
            fit: BoxFit.cover,
            height: 120,
            width: double.infinity,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              tourPackage.name,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Icon(
                  Icons.star,
                  size: 16,
                  color: Colors.yellow[700],
                ),
                const SizedBox(width: 5),
                Text(
                  tourPackage.rating.toString(),
                  style: const TextStyle(fontSize: 14),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
